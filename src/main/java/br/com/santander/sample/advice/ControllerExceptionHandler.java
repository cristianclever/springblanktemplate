package br.com.santander.sample.advice;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import br.com.santander.sample.controller.SampleController;

@ControllerAdvice
public class ControllerExceptionHandler extends ResponseEntityExceptionHandler {

	private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(SampleController.class);
	
	@ExceptionHandler({ Throwable.class })
	public ResponseEntity<Object> handleConstraintViolation(Throwable e, WebRequest request) {
		
		if(e!=null) {
			log.error("Erro:" + e.getMessage(),e);
		}
		
		return new ResponseEntity<Object>(HttpStatus.INTERNAL_SERVER_ERROR);
	}

	
	@ExceptionHandler({ NullPointerException.class })
	public ResponseEntity<Object> handleConstraintViolation(NullPointerException e, WebRequest request) {
		
		if(e!=null) {
			log.error("Erro:" + e.getMessage(),e);
		}
		
		return new ResponseEntity<Object>(HttpStatus.INTERNAL_SERVER_ERROR);
	}

	
}
