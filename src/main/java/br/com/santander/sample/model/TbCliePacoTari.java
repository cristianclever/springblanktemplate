package br.com.santander.sample.model;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TB_CLIE_PACO_TARI", schema = "FEC")
public class TbCliePacoTari implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String cdPessPaco;
	private String inPaco;

	public TbCliePacoTari() {
	}

	public TbCliePacoTari(String cdPessPaco, String inPaco) {
		this.cdPessPaco = cdPessPaco;
		this.inPaco = inPaco;
	}

	@Id
	@Column(name = "CD_PESS_PACO", unique = true, nullable = false, length = 8)
	public String getCdPessPaco() {
		return this.cdPessPaco;
	}

	public void setCdPessPaco(String cdPessPaco) {
		this.cdPessPaco = cdPessPaco;
	}

	@Column(name = "IN_PACO", nullable = false, length = 1)
	public String getInPaco() {
		return this.inPaco;
	}

	public void setInPaco(String inPaco) {
		this.inPaco = inPaco;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cdPessPaco == null) ? 0 : cdPessPaco.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TbCliePacoTari other = (TbCliePacoTari) obj;
		if (cdPessPaco == null) {
			if (other.cdPessPaco != null)
				return false;
		} else if (!cdPessPaco.equals(other.cdPessPaco))
			return false;
		return true;
	}

}
 

