package br.com.santander.sample.config;

import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.web.servlet.DispatcherServletAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import br.com.santander.sample.controller.BaseController;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration

@EnableSwagger2
public class SwaggerConfig {                                    
    
	

	
	@Bean
    public Docket api() { 
    	
    	
    	System.out.println("SwaggerConfig.api()");
    	
    	
    	
    	
    	
        return new Docket(DocumentationType.SWAGGER_2)  
        		
          .select()                                  
          .apis(RequestHandlerSelectors.basePackage(BaseController.getControllersBasePackage()))
          .paths(PathSelectors.any())                  
          .build();                                           
    }
    

}
