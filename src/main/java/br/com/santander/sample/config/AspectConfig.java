package br.com.santander.sample.config;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.context.annotation.Configuration;

import br.com.santander.sample.controller.BaseController;
import br.com.santander.sample.controller.SampleController;

@Configuration
@Aspect
public class AspectConfig {

	
	private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(SampleController.class);
	
	
	
    @Around( "execution(* br.com.santander.sample.controller.*.*(..))")
    public Object doIntercept(ProceedingJoinPoint pjp) throws Throwable {

    	if(log.isDebugEnabled()) {
    		log.debug("#> BEGIN EXECUTION:"+ pjp);
    	}
    	
    	final String signature = pjp.getSignature().toShortString();
    	
    	
    	Object retVal = null;
    	
    	try {
    		retVal = pjp.proceed();
    	}
    	catch(Throwable e) {
    		log.error("[" + signature + "][]",e);
    		throw e;
    	}
    	finally {
    		if(log.isDebugEnabled()) {
    			log.debug("#> END EXECUTION:"+ pjp);
    		}
    	}
    	
    	return retVal;

    }
    
    
}
