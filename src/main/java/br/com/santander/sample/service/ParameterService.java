package br.com.santander.sample.service;

import java.util.List;

import javax.cache.annotation.CacheResult;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.santander.sample.model.TbCliePacoTari;
import br.com.santander.sample.repository.TbCliePacoTariRepository;

@Service
public class ParameterService extends BaseTransactionService {

	@Autowired
	private TbCliePacoTariRepository repository;
	
	@CacheResult(cacheName="defaultCache")
	@Transactional(readOnly = true)
	public List<TbCliePacoTari> findAllParameters(){
		System.out.println("ParameterService.findAllParameters()");
		long count = repository.count();
		return null;
	}
	
}
