package br.com.santander.sample.service;

import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT, timeout = BaseTransactionService.TIMEOUT_GENERICO)

public class BaseTransactionService {

	/**
	 * Casos especificos deverao ser sobrescritos diretamente no metodo
	 * transacionado, exemplo TIMEOUT_ESPECIFICO
	 */

	public static final int TIMEOUT_GENERICO = 1000; // 1 segundo
}
