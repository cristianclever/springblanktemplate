package br.com.santander.sample.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import br.com.santander.sample.advice.ControllerExceptionHandler;
import br.com.santander.sample.config.PersistenceJPAConfig;
import br.com.santander.sample.controller.SampleController;
import br.com.santander.sample.repository.BaseRepository;
import br.com.santander.sample.service.BaseTransactionService;

@SpringBootApplication

@EnableAutoConfiguration
@ComponentScan(basePackageClasses= {
		PersistenceJPAConfig.class,
		SampleController.class,
		BaseTransactionService.class,
		BaseRepository.class,
		ControllerExceptionHandler.class
})

public class SampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(SampleApplication.class, args);
	}

}

