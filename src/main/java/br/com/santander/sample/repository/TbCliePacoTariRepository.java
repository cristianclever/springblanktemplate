package br.com.santander.sample.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.santander.sample.model.TbCliePacoTari;

@Repository
public interface TbCliePacoTariRepository extends JpaRepository<TbCliePacoTari, String> {

}
