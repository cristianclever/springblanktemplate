package br.com.santander.sample.controller;

import org.springframework.web.bind.annotation.CrossOrigin;


public abstract class BaseController {

	public static String getControllersBasePackage() {
		return BaseController.class.getPackage().getName();
	}
}
